//
//  AnotacoesViewController.swift
//  Notas Diarias
//
//  Created by Anna Carolina Ribeiro Mendonça on 07/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit
import CoreData

class AnotacoesViewController: UIViewController {
    
    
    @IBOutlet weak var texto: UITextView!
    var context: NSManagedObjectContext!
    var anotacao: NSManagedObject!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configuracoes iniciais
        self.texto.becomeFirstResponder()
        // atualizando anotacoes
        if anotacao != nil {
            if let textoRecuperado = anotacao.value(forKey: "texto") {
                self.texto.text = String(describing: textoRecuperado)
            }
        }else{
            self.texto.text = ""
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    @IBAction func salvar (_ sender: Any) {
        if anotacao != nil {
            self.atualizarAnotacao()
        }else{
            self.salvarAnotacao()
        }
    
        //retornar para tela principal ao salvar anotacao
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func atualizarAnotacao() {
        // configura anotacao
        anotacao.setValue(self.texto.text, forKey: "texto")
        anotacao.setValue(Date(), forKey: "data")
        
        do {
            try context.save()
            print("Sucesso ao atualizar anotação!")
        } catch let erro {
            print("Erro ao atualizar anotação: \(erro.localizedDescription)")
        }
    }
    
    func salvarAnotacao() {
        
        // criar objeto para anotacao
        let novaAnotacao = NSEntityDescription.insertNewObject(forEntityName: "Anotacao", into: context)
        
        //configurar anotacao
        novaAnotacao.setValue(self.texto.text, forKey: "texto")
        novaAnotacao.setValue(Date(), forKey: "data")
        
        do {
            try context.save()
            print("Sucesso ao Salvar a anotação!")
        } catch let erro {
            print("Erro ao Salvar anotação: \(erro.localizedDescription)" )
        }
    }
}
